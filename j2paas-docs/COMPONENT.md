# j2paas web组件

## 介绍
前端应用组件库，如果您需要编写新的组件，请参考下面的编写流程

## 一、什么是J2PaaS页面组件


J2PaaS页面框架使用ZK Web框架，其对应的每个UI对象都由widget和component组成：


**Component（java组件)：**
component是运行在服务器上的 Java 对象，它表示一个 UI 对象，该对象可以由Java 应用程序操作。component
除了不可显示的属性外，其余的具有 UI 对象的所有行为，例如setFocus、setStyle....。

**Widget(javascript小部件）:**
widget是运行在客户端上JavaScript 对象。此对象表示与用户交互的 UI 对象。因此，widget通常具有可视外观，
并处理发生在客户端的事件。


在确定J2PaaS 组件有两个部分之后，需要探讨这些部件如何相互交互，以形成完全交互式的用户体验。


## 二、组件的交互过程


component和widget携手工作，widget会捕获用户活动并向component发送适当的请求。然后，该component
与开发人员的应用程序进行交互，应用程序将适当地响应，告知widget何时应该更新。下图演示了此交互:
![ZKComDevEss_widget_component_application.png](img/component/1584672425367-12ced7de-6aa3-4f35-8573-8503e111bd1c.png)

例如，当应用程序调用 setLabel 方法更改按钮组件的标签时，将在客户端调用相应按钮widget的 setLabel 方法
以更改显示的内容（如下所示)


![ZKComDevEss_button_labels.png](img/component/1584672347529-24951bdd-2b07-432c-82b4-c5b1a38da0f0.png)


当用户单击按钮小部件时，onClick 事件将发送回服务器并通知应用程序（如下图所示）:


![ZKComDevEss_button_click.png](img/component/1584672356344-dce5382b-c2ac-4d4a-af0a-ff6266f27732.png)


除了在服务器上操作组件外，还可以控制客户端上的小部件。例如，应用程序可能会隐藏或更改客户端上的网格列的顺序，
而在服务器上运行的应用程序处理网格内容的重新加载。此技术称为服务器+客户端融合。它可用于提高响应能力并减少网络流量。


## 三、开发工具
下面是开发工具的列表。虽然可选，但它们在许多情况下是有帮助的


| 工具 | 描述 |
| --- | --- |
| ZK idea或eclipse插件 | 提供简化组件创建向导的组件开发工具 |
| Chrome、Firefox浏览器 | 提供脚本调试的工具，开发J2PaaS页面组件最常用的工具 |



1、以idea为例，安装好ZK开发工具插件，如图：


![image.png](img/component/1584628683787-20537cef-b47d-46c4-8fb3-56bdf809fdbc.png)


2、打开idea，创建maven项目，勾选从原型创建，然后选择zk component，如图：


![image.png](img/component/1584628805660-8ecf2694-6f37-4dee-9283-f22c03bf0edb.png)


然后按照提示步骤完成项目的创建，最终生成的项目结构如图：


![image.png](img/component/1584683920093-6ec950aa-4e24-40e3-a6b8-940d68230825.png)





---

[simpleLabel.rar](https://www.yuque.com/attachments/yuque/0/2020/rar/965747/1584684060145-763931dd-b563-4340-8ad9-4ac5b0164f08.rar?_lake_card=%7B%22uid%22%3A%221584684060486-0%22%2C%22src%22%3A%22https%3A%2F%2Fwww.yuque.com%2Fattachments%2Fyuque%2F0%2F2020%2Frar%2F965747%2F1584684060145-763931dd-b563-4340-8ad9-4ac5b0164f08.rar%22%2C%22name%22%3A%22simpleLabel.rar%22%2C%22size%22%3A14366%2C%22type%22%3A%22%22%2C%22ext%22%3A%22rar%22%2C%22progress%22%3A%7B%22percent%22%3A99%7D%2C%22status%22%3A%22done%22%2C%22percent%22%3A0%2C%22id%22%3A%22gLelb%22%2C%22card%22%3A%22file%22%7D) 项目源代码 [test.rar](https://www.yuque.com/attachments/yuque/0/2020/rar/965747/1585547225863-637d5a37-b43c-4424-8451-5bf3de4ac174.rar?_lake_card=%7B%22uid%22%3A%221585547226132-0%22%2C%22src%22%3A%22https%3A%2F%2Fwww.yuque.com%2Fattachments%2Fyuque%2F0%2F2020%2Frar%2F965747%2F1585547225863-637d5a37-b43c-4424-8451-5bf3de4ac174.rar%22%2C%22name%22%3A%22test.rar%22%2C%22size%22%3A6477%2C%22type%22%3A%22%22%2C%22ext%22%3A%22rar%22%2C%22progress%22%3A%7B%22percent%22%3A99%7D%2C%22status%22%3A%22done%22%2C%22percent%22%3A0%2C%22id%22%3A%22aJ39y%22%2C%22card%22%3A%22file%22%7D) 测试代码




## 四、创建一个简单的组件


#### 1、实现Component(java端组件)
如前所述，J2PaaS组件通常用 Java 编写的服务器端组件和 JavaScript 编写的基于客户端的小部件组成。首先，让我们讨论服务器端组件
的创建。组件的 Java 类必须扩展抽象组件AbstractComponent或其派生类之一。有几个派生类都提供不同级别的功能。派生类如下所示。




![ZKComDevEss_component_hierarchy.png](img/component/1584672242972-340a2b84-062f-4459-8bbd-a27d8715712f.png)
出于教程目的，我们使用 HtmlBasedComponent，这是基于 HTML 的组件的基础类。要实现组件类，我们需要决定 类名称：


- 让我们将其命名为com.epclouds.web.ext.zul.SimpleLabel
- 组件必须要有支持的属性。在这种情况下，我们希望实现一个称为value的属性，即客户端的可视内容。让我们来实现组件属性。
- 当组件首次加载到页面时，将调用 render方法（org.zkoss.zk.ui.sys.sys.ContentRendererer），该方法以关联的值

发送到客户端，用以创建对等小部件的所有属性。必须render所有必需的属性，以便客户端可以使用相同的属性集
创建对等小部件。实现渲染属性（org.zkoss.zk.ui.sys.ContentRenderer）非常简单。如下所示，只需调用
super.renderProperty以呈现所有继承的属性（如宽度和高度），然后调用render方法来呈现在此类中定义的属性。


```java
package com.epclouds.web.ext.zul;
public class SimpleLabel extends org.zkoss.zk.ui.HtmlBasedComponent {
    private String _value = ""; //数据成员，表示组件与客户端映谢关联的对象
      
    public String getValue() {
        return _value;
    }
    
    public void setValue(String value) {
            if (!_value.equals(value)) {
                _value = value;
                smartUpdate("value", _value);//当值发生改变时，组件会通知对应的小部件，同时小部件会操纵DOM树以进行更新
            }
    }
   
    /**
    *在组件生成时调用，如果有多个属性匹对到小部件的多个属性，在这个方法里就需要有多个render
    *
    */
    protected void renderProperties(org.zkoss.zk.ui.sys.ContentRenderer renderer)throws java.io.IOException {
        super.renderProperties(renderer);
        render(renderer, "value", _value);
    }
 }

```


#### 2、实现Widget(javascript小部件)
小部件类必须从 Widget 或其派生类之一扩展。有几个继承级别的实现可用。具体实现类如下图所示


![ZKComDevEss_widget_hierarchy.png](img/component/1584672281389-0be64b76-63c8-49f9-818c-40702fc2a566.png)


从上图可以看出，与Component派生类对比，每个widget刚好对应一个component实现，所以，在确认您所需要的组件时先选择好
对应的component和widget实现。在本教程中，在继续之前，我们需要决定widget小部件类的名称。com.epclouds.SimpleLabel。


```javascript
com.epclouds.SimpleLabel = zk.$extends(zk.Widget, {
      _value : '', // 绑定的值
      getValue : function() {
        return this._value;
      },
      setValue : function(value) {
        if (this._value != value) {
          this._value = value;
          if (this.desktop)
            this.$n().innerHTML = zUtl.encodeXML(value);
        }
      }
});
```


- 小部件实现必须放在独立的 JavaScript 文件中。且必须放置在目录/web/js/package-path下，因此，在本教程中，package路径是

com.epclouds，小部件名称是SimpleLabel，组合起来的路径是/web/js/com/epclouds/SimpleLabel.js。

- 在设置属性状态的功能后，我们现在需要创建组件视图。可以使用两种方法来完成，第一种是称为mold，它使小部件具有多个显示类型，

开发人员或用户可以在多个待选项中选择。第二种是仅支持一种视图类型的redraw方法。首先，让我们讨论如何实施mold。

- 小部件可以有多个mold。每个mold都需要放在一个独立的 JavaScript 文件中。在名为mold的子目录下。在本教程中,此示例中目录的完整

路径将是/web/js/com/epclouds/mold/simple-label.js，simple-label.js内容如下：


```javascript
function (out) {
  out.push('<span', this.domAttrs_(), '>', this.getValue(), '</span>');
}

```
如上所述，mold实际上是一种 JavaScript 方法。更确切地说，它是小部件类的方法成员（名称由 ZK 客户端引擎自动分配），因此您可
以使用此访问小部件对象。mold方法采用一个命名的参数，它就像Java中的编写器一样。out 对象至少实现push和unshift方法，将内容写入
末尾或开头。默认情况下，它是一个数组，但客户端应用程序可能使用不同类型的对象。domAttrs_是从 Widget继承的（受保护）方法。它
返回所有必需的 HTML 属性，如样式、ID 等。如果需要，可以覆盖它。


- 如果我们不需要每个组件有多个样式，我们可以直接实现redraw方法，这也是创建组件视图的另外一种方式。

当小部件加载到 DOM 树时，Widget.redraw被调用以生成 HTML 内容。例如，假设您要使用HTML SPAN标记来容纳内容，我们可以按照如下
方式执行操作。


```javascript
redraw: function (out) {
  out.push('<span', this.domAttrs_(), '>', this.getValue(), '</span>');
} 
```
在这种情况下，我们将重写该函数，以提供不使用mold的重绘的一个实现。


#### 3、创建配置文件


为了让上述开发的SimpleLabel组件可以在应用系统中使用，我们需要提供2个名为lang-addon.xml和zk.wpd的配置文件。


- lang-addon.xml:

此文件内容主要包含描述component与widget之间关联以及组件的定义，这部分必须保存在/metainfo/zk/lang-addon.xml文件中。


```xml
<language-addon>
  <addon-name>simplelabel</addon-name><!--插件名称，必须是唯一名称，一个插件定义文件下面可以包括多个组件的定义-->
  <language-name>xul/html</language-name><!--插件使用的语言名称，例如xul/html、zhtml-->
 
      <component><!--定义组件-->
        <component-name>simplelabel</component-name><!--组件名称，也必须是唯一的-->
        <component-class>com.epclouds.web.ext.zul.SimpleLabel</component-class><!--组件java端实现类-->
        <widget-class>com.epclouds.SimpleLabel</widget-class><!--组件javascript端实现类-->
        <mold><!--定义视图显示方式，一个组件可以有多个mold，例如为了让zk的grid组件显示成bootstrap的table样式，只需要扩展一个mold就可以实现-->
          <mold-name>default</mold-name><!--作为组件默认的显示方式-->
          <mold-uri>mold/simple-label.js</mold-uri><!--对应的html视图创建文件-->
          <css-uri>css/simple-label.css.dsp</css-uri><!--关联的css文件-->
        </mold>
  </component>
</language-addon>

```


- zk.wpd

WPD(Widget Package Descriptor)，zk.wpd是一个描述小部件的package信息，包括小部件实现类和外部关联的JavaScript文件，WPD必须保存在
组件package目录下面，例如web/js/com/epclouds/zk.wpd，下面是zk.wpd内容：


```xml
<package name="com.epclouds" language="xul/html" depends="zul.mesh,zul.menu">
  <script src="codemirror.min.js"/>
  <widget name="SimpleLabel"/>
</package>

```


#### 4、事件控制

下一个逻辑步骤是向组件添加事件。为此，我们将向 SimpleLabel 添加更多功能。将添加新的 div，单击该 div 时应清除显示的文本并触发名为onClear的自定义事件。
这意味着，首先我们需要改变标签的mold，以包括可用作目标的 div。以下代码满足此要求：


```javascript
function (out) {
 out.push('<span', this.domAttrs_(), '><div id="value" style="float:left;">', this.getValue(), '</div><div id="target" style="float:left;cursor: pointer; cursor: hand;height:20px;width:20px;background-color:red;"></div></span>');
}
```
正如您所看到的，我们现在通过引入两个 div 标记将标签的内容拆分为两个，一个将用于显示值，另一个将用作单击目标。


怎样实现单击事件呢，接下来我们需要实现下面的两个步骤：


- 重载[bind_(Desktop, Skipper, Array)](http://www.zkoss.org/javadoc/latest/jsdoc/zk/Widget.html#bind_(zk.Desktop,%20zk.Skipper,%20_global_.Array))和 [unbind_(Skipper, Array)](http://www.zkoss.org/javadoc/latest/jsdoc/zk/Widget.html#unbind_(zk.Skipper,%20_global_.Array))方法



下图概述bind_方法的工作过程：


![](img/component/1584671482685-5292c81b-6dc3-4db3-8afb-054e44b96ec0.png)

下图概述unbind_方法的工作过程：

![](img/component/1584671553210-590dfe4c-6078-49df-a7bc-339982ffa684.png)


- 注册适合的侦听器(listener)



注册侦听器是许多编程语言和事件驱动应用程序的基础。 ZK作为事件驱动框架也不例外。为了完成我们按钮事件的目标，我们需要做两件事：


a. 获取单击事件的目标元素
首先，让我们专注于捕获目标 div 上的点击事件。为此，我们需要在bind_方法中注册适当的侦听器，并在unbind_方法中删除侦听器，以
避免任何内存泄漏。下面是执行此操作的代码
b.当目标元素被点击时触发自定义的onClear事件

```javascript
bind_ : function(evt) {
        this.$supers('bind_', arguments);
        this.domListen_(this.$n().lastChild, "onClick", '_doClear');
},
 
unbind_ : function(evt) {
        this.domUnlisten_(this.$n().lastChild, "onClick", '_doClear');
        this.$supers('unbind_', arguments);
},
```


关键是 domListen 方法，该方法将目标作为第一个参数，在这种情况下，我们传递lastChild作为我们的目标，您要侦听的事件的名称作为第二
个参数，最后是回调的名称。请注意，您还需要执行$supers以便父类可以成功注册和删除其事件。现在，我们需要继续触发我们的自定义"onClear"
事件。让我们来看看如何做到这一点。
怎样触发自定义事件，关键在于注册事件的回调。在我们的案例中，我们注册了名为"_doClear"的回调。在此_doClear方法，我们需要根据小部
件状态清除/还原文本，并触发 onClear 方法。代码如下：


   
```javascript
_doClear: function(evt) {

        this._cleared = !(this._cleared);
         
        if(this._cleared) {
            this.$n().firstChild.innerHTML = this._value;
        } else {
            this.$n().firstChild.innerHTML = "";
        }
         
        this.fire("onClear", {cleared: this._cleared});//触发自定义事件
}
```
在这里，我们定义一个名为_cleared的数据成员，其中包含应用程序的状态。根据状态，该方法显示或清除显示的值。然后，将触发方法onClear并发送
清除状态以及触发事件的说明。客户端小部件现在将与服务器端的组件进行通信，俩者之间的交互流程如下：
![ZKComDevEss_fire_event.png](img/component/1584673615876-2caf5b42-62bb-4185-a10d-2ab460612903.png)
上面的图像演示 onClear 事件发送到服务器并处理。要执行的代码位于组件的 Java 文件 SimpleLabel.java 中，如下所示。

```java
public void service(org.zkoss.zk.au.AuRequest request, boolean everError) {
        final String cmd = request.getCommand();
 
        if (cmd.equals(ClearEvent.NAME)) {
            ClearEvent evt = ClearEvent.getClearEvent(request);
            _cleared = evt.getCleared();
            Events.postEvent(evt);
        } else
            super.service(request, everError);
}
```


在这里，ClearEvent 实际上是为此组件的目的创建的完全自定义的事件。使用静态方法 getClearEvent 创建的事件如下所示。


```java
public static final ClearEvent getClearEvent(AuRequest request) {
            final Component comp = request.getComponent();
            final Map data=request.getData();
             
            boolean cleared = AuRequests.getBoolean(data, "cleared");
            return new ClearEvent(request.getCommand(), comp, cleared);
}
```


现在，我们已经处理了如何在客户端和服务器之间传输数据的过程。当然，在构建组件时，我们还需要指定服务器端侦听器来调用特定事件，
为了使开发人员能够在服务器端注册事件侦听器，我们需要声明事件。我们可以使用名为 addClientEvent 的功能执行此操作。


```javascript
static {
        addClientEvent(SimpleLabel.class, ClearEvent.NAME, 0);//注册自定义事件
}
```




## 五、打包
将组件打包为 Jar 文件将使其易于部署。本节演示了它的步骤和要求。


- 配置(Configurations)
- 服务端组件类(Component classes)
- 客户端小部件(Widget Resources)
- 一些相关的静态资源(Static Resources)



以下会以zul.jar为例，总体目录结构如下：


![Jar_File_summary.jpg](img/component/1584674639982-4d5a3aa3-b765-4f0c-b3dc-d0ea3281f995.jpeg)
#### 1、配置(Configurations)   
    

文件结构
- /META-INF/
    - MANIFEST.MF
- /metainfo/
    - mesg/
        - msg<jar name>.properties**(可选)**
        - msg<jar name>_<locale>.properties **(可选 ...)**
    - xml/
        - <component-name>.xsd **(可选)**
    - zk/
        - lang.xml **(可选)**
        - lang-addon.xml **(可选)**
          文件描述
- /META-INF/
    - MANIFEST.MF
        - 文件信息
- /metainfo/
    - mesg/
        - msg<jar name>.properties
            - _多国语言资源文件，默认是 msg<jar name>.properties._
              [ZK Internationalization](https://www.zkoss.org/wiki/ZK_Developer%27s_Reference/Internationalization/Warning_and_Error_Messages)
    - xml/
        - <jar-name>.xsd
            - 标记组件结构文件
    - tld
        - config.xml
            - _jsp标签定义_
    - zk/
        - lang-addon.xml
            - _插件定义文件._
        - lang.xml
            - 带有命名空间的语言定义文件_(例如. [http://www.w3.org/1999/xhtml](http://www.w3.org/1999/xhtml) for zhtml_
              ![Jar_File_configuration1.jpg](img/component/1584674890894-6e756e35-1c6f-480e-af27-029a9d7ea592.jpeg)




#### 2、服务端组件类(Component classes)



文件结构
- /包目录- 类文件


文件描述
- 组件的java类，和正常的java文件一样。
  ![Jar_File_component1.jpg](img/component/1584675417131-406c83f6-87d0-43df-934b-ca295bc8a1c3.jpeg)




#### 3、客户端小部件(Widget Resources)



文件结构
- /web/
    - js/
        - <component-package>/
            - mold/
                - <widget-mold-js-file>
            - css/
                - <widget-css-dsp-file>** (可选)**
                - <widget-css-file> **(可选)**
            - <widget-class-js-file>
            - /zk.wpd

文件描述
- /web/
    - js/
        - <component-package>/
            - mold/
                - <widget-mold-js-file> (例如. simple-label.js )
                   - 小部件mold文件，你可以在这里写小部件的html与javascript函数。
            - css/
                - <widget-css-dsp-file> (例如. simple-label.css.dsp )
                   - css dsp文件，在dsp中，你可以使用一些变量与zk环境来写它
                - <widget-css-file> (例如. simple-label.css )
                   - _纯css文件._
            - <widget-class-file> (例如. Simple-label.js)
               - 小部件实现类
            - zk.wpd
                - 在此处定义组件的包和小部件，以及与其他包的依赖项

![Jar_File_widget1.jpg](img/component/1584675596242-657d63d8-2012-46ab-b086-e557b0b43e9e.jpeg)




#### 4、一些相关的静态资源(Static Resources)



文件结构
- /web/
    - <component-package> /**(可选)**
       - css /**(可选)**
          - <css files>**(可选)**
          - zk.wcs**(可选)**
       - img /**(可选)**
          - <img files>**(可选)**

文件描述
- /web/
    - <component-package>/
        - css/
            - <css files>
               - 对于一些可能需要的静态css文件
            - zk.wcs
                - 允许为特定语言配置 CSS 文件
        - img/
            - <img files>
               - 这是一些图像文件的文件夹，您可以在 xxx.css.dsp 文件中通过_ ${c:encodeURL('~./img/<component-package>/xxx.png')}访问它们_
![Jar_File_static_resources1.jpg](img/component/1584675949400-0d8224d0-359c-4a16-aab5-5c03d3a3f844.jpeg)





