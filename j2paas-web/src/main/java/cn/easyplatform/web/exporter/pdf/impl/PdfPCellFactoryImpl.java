/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.exporter.pdf.impl;

import cn.easyplatform.lang.Files;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.web.exporter.pdf.JoinCellEvent;
import cn.easyplatform.web.exporter.pdf.PdfPCellFactory;
import com.itextpdf.text.*;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;

import java.io.IOException;
import java.net.MalformedURLException;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PdfPCellFactoryImpl implements PdfPCellFactory {

    protected BaseColor defaultBorderColor = WebColors.getRGBColor("#CFCFCF");
    protected BaseColor defaultOddRowBackgroundColor = WebColors.getRGBColor("#F7F7F7");
    protected BaseColor defaultFooterBackgroundColor = WebColors.getRGBColor("#F9F9F9");

    protected JoinCellEvent headerEventJoiner = new JoinCellEventImpl(new PdfPCellEvent() {

        @Override
        public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases) {
            PdfContentByte pdfContentByte = canvases[PdfPTable.BACKGROUNDCANVAS];
            //URL resource = PdfExporter.class.getResource("column-bg.png");
            Image image;
            try {
                image = Image.getInstance(Streams.readBytesAndClose(PdfPCellFactory.class.getResourceAsStream("/support/img/column-bg.png")));
                pdfContentByte.addImage(image, cell.getWidth(), 0, 0, position.getHeight(), position.getLeft(), position.getBottom());
            } catch (BadElementException e) {
                throw new RuntimeException(e);
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (DocumentException e) {
                throw new RuntimeException(e);
            }
        }
    });
    protected JoinCellEvent groupEventJoiner = new JoinCellEventImpl(new PdfPCellEvent() {

        @Override
        public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases) {
            PdfContentByte pdfContentByte = canvases[PdfPTable.BACKGROUNDCANVAS];
            Image image;
            try {
                //URL resource = PdfExporter.class.getResource("group_bg.gif");
                image = Image.getInstance(Streams.readBytesAndClose(PdfPCellFactory.class.getResourceAsStream("/support/img/group_bg.gif")));
                pdfContentByte.addImage(image, cell.getWidth(), 0, 0, position.getHeight(), position.getLeft(), position.getBottom());
            } catch (BadElementException e) {
                throw new RuntimeException(e);
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (DocumentException e) {
                throw new RuntimeException(e);
            }
        }
    });

    protected JoinCellEvent groupfootEventJoiner = new JoinCellEventImpl(new PdfPCellEvent() {

        @Override
        public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases) {
            PdfContentByte pdfContentByte = canvases[PdfPTable.BACKGROUNDCANVAS];
            Image image;
            try {
                //URL resource = PdfExporter.class.getResource("groupfoot_bg.gif");
                image = Image.getInstance(Streams.readBytesAndClose(PdfPCellFactory.class.getResourceAsStream("/support/img/groupfoot_bg.gif")));
                pdfContentByte.addImage(image, cell.getWidth(), 0, 0, position.getHeight(), position.getLeft(), position.getBottom());
            } catch (BadElementException e) {
                throw new RuntimeException(e);
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (DocumentException e) {
                throw new RuntimeException(e);
            }
        }
    });

    @Override
    public PdfPCell getHeaderCell() {
        PdfPCell cell = new PdfPCell();
        cell.setPaddingTop(8);
        cell.setPaddingRight(4);
        cell.setPaddingBottom(7);
        cell.setPaddingLeft(6);

        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

        cell.setBorderColor(defaultBorderColor);
        cell.setCellEvent((PdfPCellEvent) headerEventJoiner);
        return cell;
    }

    @Override
    public PdfPCell getCell(boolean isOddRow) {
        PdfPCell cell = getDefaultPdfPCell();
        if (isOddRow)
            cell.setBackgroundColor(defaultOddRowBackgroundColor);
        return cell;
    }

    @Override
    public PdfPCell getGroupCell() {
        PdfPCell cell = getDefaultPdfPCell();
        cell.setCellEvent((PdfPCellEvent) groupEventJoiner);
        return cell;
    }

    @Override
    public PdfPCell getGroupfootCell() {
        PdfPCell cell = new PdfPCell();
        cell.setBorderColor(defaultBorderColor);
        cell.setPaddingTop(5);
        cell.setPaddingRight(4);
        cell.setPaddingBottom(5);
        cell.setPaddingLeft(6);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

        cell.setCellEvent((PdfPCellEvent) groupfootEventJoiner);
        return cell;
    }

    @Override
    public PdfPCell getFooterCell() {
        PdfPCell cell = new PdfPCell();
        cell.setBorderColor(defaultBorderColor);
        cell.setPaddingTop(5);
        cell.setPaddingRight(10);
        cell.setPaddingBottom(5);
        cell.setPaddingLeft(9);
        cell.setBackgroundColor(defaultFooterBackgroundColor);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        return cell;
    }

    @Override
    public Image getImage(boolean isOddRow, byte[] content) {
        try {
            Image image = Image.getInstance(content);
            image.setBorderColor(defaultBorderColor);
            if (isOddRow)
                image.setBackgroundColor(defaultOddRowBackgroundColor);
            return image;
        } catch (Exception e) {
            return null;
        }
    }

    protected PdfPCell getDefaultPdfPCell() {
        PdfPCell cell = new PdfPCell();
        cell.setBorderColor(defaultBorderColor);
        cell.setPaddingTop(4);
        cell.setPaddingRight(4);
        cell.setPaddingBottom(4);
        cell.setPaddingLeft(6);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        return cell;
    }
}