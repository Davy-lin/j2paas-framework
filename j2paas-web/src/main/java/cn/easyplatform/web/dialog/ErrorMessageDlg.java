/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.dialog;

import cn.easyplatform.messages.vos.EnvVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.web.contexts.Contexts;
import cn.easyplatform.web.listener.LogoutListener;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
class ErrorMessageDlg implements MessageDlg {

    private String code;

    private String message;

    ErrorMessageDlg(IResponseMessage<?> resp) {
        this.code = resp.getCode();
        this.message = (String) resp.getBody();
    }

    @Override
    public void show() {
        if (code.equals("E000"))// 用户被踢出
            Messagebox.show(message, code, Messagebox.OK, Messagebox.ERROR,
                    new EventListener<Event>() {
                        @Override
                        public void onEvent(Event event) throws Exception {
                            Clients.confirmClose("");
                            EnvVo env = Contexts.getEnv();
                            if (env != null)
                                LogoutListener.logout(env);
                        }
                    });
        else
            Messagebox.show(message, code, Messagebox.OK, Messagebox.ERROR);
    }
}
