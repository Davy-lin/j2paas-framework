/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.controller;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.CheckRequestMessage;
import cn.easyplatform.messages.vos.CheckVo;
import cn.easyplatform.spi.service.IdentityService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.web.contexts.Contexts;
import cn.easyplatform.web.service.ServiceLocator;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Composer;
import org.zkoss.zul.Textbox;

import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CheckUserController implements Composer<Component> {

	private int tryTimes = 1;

	@Override
	public void doAfterCompose(final Component comp) throws Exception {
		@SuppressWarnings("unchecked")
		final Map<String, Object> args = (Map<String, Object>) Executions
				.getCurrent().getArg();
		Component submit = comp.query("button");
		comp.getFellow("password").addForward(Events.ON_OK, submit,
				Events.ON_CLICK);
		submit.addEventListener(Events.ON_CLICK, new EventListener<Event>() {

			@Override
			public void onEvent(Event evt) throws Exception {
				Textbox user = (Textbox) evt.getTarget().getFellow("user");
				if (Strings.isBlank(user.getValue())) {
					user.setFocus(true);
					return;
				}
				if (Contexts.getUser().getId()
						.equalsIgnoreCase(user.getValue())) {
					user.setFocus(true);
					throw new WrongValueException(user, Labels
							.getLabel("user.check.same"));
				}
				Textbox password = (Textbox) evt.getTarget().getFellow(
						"password");
				if (Strings.isBlank(password.getValue())) {
					password.setFocus(true);
					return;
				}
				IdentityService is = ServiceLocator
						.lookup(IdentityService.class);
				IResponseMessage<?> resp = is.check(new CheckRequestMessage(
						(String) args.get("id"), new CheckVo(user.getValue(),
								password.getValue(), (String) args
										.get("toField"), (String[]) args
										.get("roles"))));
				if (resp.isSuccess()) {
					Boolean success = (Boolean) resp.getBody();
					if (!success.booleanValue() && tryTimes < 4) {
						tryTimes++;
						throw new WrongValueException(evt.getTarget(), Labels
								.getLabel("user.check.error"));
					}
				}
				Events.postEvent(new Event("onCheckUser", comp, resp));
				evt.getTarget().getRoot().detach();
			}
		});
	}

}
