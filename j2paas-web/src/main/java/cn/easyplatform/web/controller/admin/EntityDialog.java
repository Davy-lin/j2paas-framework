/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.controller.admin;

import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.vos.addon.SearchVo;
import cn.easyplatform.spi.service.AddonService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.web.dialog.MessageBox;
import cn.easyplatform.web.service.ServiceLocator;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class EntityDialog extends Window implements EventListener<Event> {

    private PropertySetCallback<String[]> cb;

    private Listbox listbox;

    public EntityDialog(PropertySetCallback<String[]> cb) {
        this.cb = cb;
        setWidth("60%");
        setHeight("60%");
        setTitle(Labels.getLabel("admin.project.entitytable"));
        setClosable(true);
        setMaximizable(true);
        setBorder("normal");
    }

    public void showDialog(Page page, SearchVo vo) {
        listbox = new Listbox();
        listbox.setHflex("1");
        listbox.setVflex("1");
        Listhead head = new Listhead();
        Listheader header = new Listheader("ID");
        head.appendChild(header);
        header = new Listheader(Labels.getLabel("admin.project.name"));
        head.appendChild(header);
        header = new Listheader(Labels.getLabel("admin.project.desp"));
        head.appendChild(header);
        header = new Listheader(Labels.getLabel("debug.header.type"));
        head.appendChild(header);
        header = new Listheader(Labels.getLabel("debug.header.subtype"));
        head.appendChild(header);
        listbox.appendChild(head);
        IResponseMessage<?> resp = ServiceLocator.lookup(AddonService.class).searchEntity(new SimpleRequestMessage(vo));
        if (resp.isSuccess()) {
            createItems((List<String[]>) resp.getBody());
            listbox.setParent(this);
            setPage(page);
            setSizable(true);
            setPosition("center,top");
            doHighlighted();
        } else {
            MessageBox.showMessage(resp);
            this.detach();
        }
    }

    private void createItems(List<String[]> items) {
        for (String[] item : items) {
            Listitem li = new Listitem();
            for (String label : item)
                li.appendChild(new Listcell(label));
            li.setValue(item);
            li.addEventListener(Events.ON_DOUBLE_CLICK, this);
            li.setParent(listbox);
        }
    }

    @Override
    public void onEvent(Event event) throws Exception {
        String[] item = listbox.getSelectedItem().getValue();
        cb.setValue(item);
        this.detach();
    }
}
