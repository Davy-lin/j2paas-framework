/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.list.group;

import cn.easyplatform.messages.vos.datalist.ListGroupVo;
import cn.easyplatform.messages.vos.datalist.ListHeaderVo;
import cn.easyplatform.type.ListRowVo;
import cn.easyplatform.web.task.zkex.ListSupport;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Treeitem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GroupTreeModel extends TreeModel {

    public static GroupTreeModel cast(ListSupport support, ListGroupVo group) {
        return createGroup(support, group, null, 0);
    }

    private static GroupTreeModel createGroup(ListSupport support, ListGroupVo g, TreeModel parent, int level) {
        GroupTreeModel group = new GroupTreeModel(support, g.getFields());
        group.level = level + 1;
        group.name = g.getName();
        group.title = g.getTitle();
        group.style = g.getStyle();
        if (parent != null)
            parent.addGroup(group);
        if (g.getChild() != null)
            createGroup(support, g.getChild(), group, group.level);
        return group;
    }

    private Component parentNode;

    private List<Treeitem> groupNodes;

    private List<GroupValue> groupValues;

    private ListRowVo prevRow;

    public GroupTreeModel(ListSupport support, String[] fields) {
        super(support, fields);
        groupNodes = new ArrayList<Treeitem>();
        groupValues = new ArrayList<GroupValue>();
    }

    @Override
    public <T extends Component> T createGroup(ResultHelper rh, int showLevel) {
        return null;
    }

    @Override
    public void clear() {
        this.groupNodes.clear();
        this.groupValues.clear();
        prevRow = null;
        for (ListHeaderVo hv : headers) {
            if (hv.isTotal())
                hv.reset();
        }
        super.clear();
    }

}
