/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task;

import cn.easyplatform.web.task.event.EventEntry;
import cn.easyplatform.web.task.event.FieldEntry;
import cn.easyplatform.web.task.support.ManagedComponents;
import org.zkoss.zk.ui.Component;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface OperableHandler extends EventSupport, ManagedComponents {

    /**
     * 刷新指定的栏位组件,当为空时刷新所有的组件
     *
     * @param entry
     */
    void refresh(EventEntry<FieldEntry> entry);

    /**
     * 新建
     *
     * @param entry
     */
    void create(EventEntry<String> entry);

    /**
     * 复制
     *
     * @param entry
     */
    void copy(EventEntry<String> entry);

    /**
     * 删除
     *
     * @param entry
     */
    void delete(EventEntry<String> entry);

    /**
     * 编辑
     *
     * @param entry
     */
    void edit(EventEntry<String> entry);

    /**
     * 保存
     *
     * @param entry
     */
    void save(EventEntry<Boolean> entry);

    /**
     * 获取页面权限
     *
     * @return
     */
    String[] getAccess();

    /**
     * 更新客户端栏位值到服务器
     *
     * @param comps
     */
    void update(String... comps);

    /**
     * @return
     */
    String getTaskId();


    /**
     * @return
     */
    Component getComponent();

    /**
     * @return
     */
    String getProcessCode();

    /**
     * @param code
     */
    void setProcessCode(String code);

    /**
     * @return
     */
    Component getContainer();
}
