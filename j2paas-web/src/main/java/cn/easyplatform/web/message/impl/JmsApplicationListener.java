/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.message.impl;

import cn.easyplatform.spi.listener.ApplicationListener;
import cn.easyplatform.spi.listener.event.AppEvent;
import cn.easyplatform.spi.listener.event.Event;
import cn.easyplatform.web.WebApps;
import cn.easyplatform.web.message.AbstractJmsConsumer;
import cn.easyplatform.web.message.entity.Destination;

import javax.jms.ConnectionFactory;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import java.io.Serializable;

/**
 * 系统内置应用侦听者
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class JmsApplicationListener extends AbstractJmsConsumer implements ApplicationListener {

    public JmsApplicationListener(ConnectionFactory connectionFactory, Destination dest) {
        super(connectionFactory, dest);
    }

    @Override
    public void onMessage(Message message) {
        if (message instanceof ObjectMessage) {
            ObjectMessage msg = (ObjectMessage) message;
            try {
                if (msg.getStringProperty("uid") == null)
                    publish(msg.getStringProperty("pid"), destination.getName(), msg.getObject());
                else
                    publish(msg.getStringProperty("pid"), destination.getName(), msg.getObject(), msg.getStringProperty("uid").split(","));
            } catch (Exception e) {
                if (log.isErrorEnabled())
                    log.error("onMessage", e);
            }
        }
    }


    @Override
    public void publish(String projectId, String topic, Serializable msg, String... toUsers) {
        if (!(msg instanceof Event))
            msg = new AppEvent(topic, msg);
        WebApps.me().publish(projectId, toUsers, (Event) msg);
    }

    @Override
    public void send(String projectId, String topic, Serializable msg, String... toUsers) {
    }
}
