/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.upcamera;


import cn.easyplatform.lang.Strings;
import cn.easyplatform.web.ext.Assignable;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.math.NumberUtils;
import org.zkoss.zk.au.AuRequest;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.impl.XulElement;

import java.util.Map;


public class Upcamera extends XulElement implements Assignable {

	public static final String ON_Save = "onSave";//用作important事件调取service

	static {
		addClientEvent(Upcamera.class, "onSave", CE_IMPORTANT);
	}

	private int maxCount;
	private String type;
	private Object value = "";


	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		if(!Strings.isBlank((String)value)) {
			this.value = value;
		}
	}

	public int getMaxCount() {
		return maxCount;
	}

	public void setMaxCount(String maxCount) {
		if(!Strings.isBlank(maxCount) && NumberUtils.isNumber(maxCount)){
			int count = Integer.parseInt(maxCount);
			if(count>0){
				this.maxCount=count;
			}else{
				this.maxCount=1;
			}
		}else{
			this.maxCount=9;
		}
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		if(!Strings.isBlank(type)) {
			this.type = type;
			if(type.equals("invoke")){
				this.setSclass("invoke-gallery_div");
			}
		}
	}

	//super//
	protected void renderProperties(org.zkoss.zk.ui.sys.ContentRenderer renderer)
	throws java.io.IOException {
		super.renderProperties(renderer);

		render(renderer, "maxCount", maxCount);
		render(renderer, "type", type);

	}

	public void service(AuRequest request, boolean everError) {
		String cmd = request.getCommand();
		final Object data = request.getData().get("data");
		this.setValue(JSON.toJSONString(data));
		if (cmd.equals(Events.ON_CLICK)) {
			if(null!=data && data!="") {
				Event evt = new Event(cmd, this, JSON.toJSONString(data));
				Events.postEvent(evt);
			}
		}
	}

	public String getZclass() {
		return (this._zclass != null ? this._zclass : "z-upcamera");
	}

	public void getAllImg() {
		Clients.evalJavaScript("zk.$('"+this.getUuid()+"').getAllImg()");
	}

	public void execute() {
		Clients.evalJavaScript("zk.$('"+this.getUuid()+"').execute()");
	}

	public void show(String id) {
		Clients.evalJavaScript("zk.$('"+this.getUuid()+"').show('"+id+"')");
	}

}

