/* CollectionTemplateResolver.java

	Purpose:
		
	Description:
		
	History:
		Wed Sep 23 14:28:50 CST 2015, Created by chunfu

Copyright (C)  2015 Potix Corporation. All Rights Reserved.

This program is distributed under LGPL Version 2.1 in the hope that
it will be useful, but WITHOUT ANY WARRANTY.
*/
package org.zkoss.zuti.zul;

import org.zkoss.zk.ui.util.Template;

/**
 * Resolver for {@link Template}, and is only used in {@link CollectionTemplate}
 *
 * @author chunfu
 */
public interface CollectionTemplateResolver<T> {
	/**
	 * Resolve the template by evaluating the variable reference from model in runtime.</p>
	 * @param t variable reference
	 * @return Template
	 */
	public Template resolve(T t);
}
