/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.calendar.api;

import java.util.Date;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface CalendarEvent {
	
	/**
	 * @return
	 */
	public Object getId();
	
	/**
	 * Returns the beginning date of the calendar event.
	 * <p>
	 * Note: never null
	 */
	public Date getBeginDate();

	/**
	 * Returns the end date of the calendar event. (exclusive)
	 * <p>
	 * Note: never null
	 */
	public Date getEndDate();

	/**
	 * Returns the title of the calendar event.
	 * <p>
	 * Note: never null
	 */
	public String getTitle();
	
	/**
	 * Returns the content of the calendar event.
	 * <p>
	 * Note: never null
	 */
	public String getContent();

	/**
	 * Returns the color of the header in the calendar event.
	 * Only allows the value being recognized by CSS. 
	 * <p>
	 * Note: never null
	 */
	public String getHeaderColor();

	/**
	 * Returns the color of the content in the calendar event.
	 * Only allows the value being recognized by CSS. 
	 * <p>
	 * Note: never null
	 */
	public String getContentColor();

	/**
	 * Returns the zclass of the calendar event.
	 * <p>
	 * Note: never null
	 */
	public String getZclass();

	/**
	 * Returns whether the calendar event is locked or not.
	 */
	public boolean isLocked();
}
