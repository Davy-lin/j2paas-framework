/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.bpm.event;

import java.io.Serializable;
import java.util.Map;

import org.zkoss.zk.au.AuRequest;
import org.zkoss.zk.au.AuRequests;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class NodeEvent extends Event {

	public final static String ON_NODE = "onNode";

	/**
	 * @param name
	 * @param target
	 * @param data
	 */
	public NodeEvent(String name, Component target, Object data) {
		super(name, target, data);
	}

	public static final NodeEvent getNodeEvent(AuRequest request) {
		final Component comp = request.getComponent();
		final Map<String, Object> data = request.getData();
		NodeVo nv = new NodeVo((String) data.get("tn"), AuRequests.getInt(data,
				"x", 0), AuRequests.getInt(data, "y", 0));
		return new NodeEvent(request.getCommand(), comp, nv);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static class NodeVo implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private String nodeName;

		private int posX;

		private int posY;

		public NodeVo(String nodeName, int posX, int posY) {
			this.nodeName = nodeName;
			this.posX = posX;
			this.posY = posY;
		}

		public String getNodeName() {
			return nodeName;
		}

		public int getPosX() {
			return posX;
		}

		public int getPosY() {
			return posY;
		}
	}
}
