/*
 * create by 陈云亮(shiny_vc@163.com)
 */
echarts.ECharts = zk.$extends(zul.Widget, {
    _engine: null,
    _theme: null,
    _options: null,
    _map: null,
    _singleFlag:true,//用在防止触发多次

    _getDataURL:false, //是否获取图片base64
    _seriesIndex:-1, //高亮目标系列
    _dataIndex:-1, //高亮目标节点
    _always:false, //是否持续高亮
    _getGraphItemLayouts: false, //是否获取关系图节点坐标

    $define: {
        theme: _zkf = function () {
            if (this._engine) {
                this._engine.dispose();
                this._engine = null;
                this._init();
            }
        },
        map: _zkf
    },

    setOptions: function (v) {
        this._options = v;
        if (this._engine) {
            this._engine.setOption(jq.evalJSON(this._options));
        }
    },
    getOptions: function () {
        return this._engine.getOption();
    },

    redraw: function (out) {
        out.push('<div ', this.domAttrs_(), '></div>');
    },
    bind_: function () {
        this.$supers(echarts.ECharts, 'bind_', arguments);
        zWatch.listen({onSize: this});
        var wgt = this;
        if (this._map && !echarts.getMap(this._map)) {
            jq.getScript(zk.ajaxURI("/web/js/echarts/ext/map/" + this._map + ".js", {au: true}))
                .done(function () {
                    wgt._init();
                });
        } else {
            setTimeout(function () {
                wgt._init()
            }, 50);
        }
    },

    _init: function () {
        this._engine = echarts.init(this.$n(), this._theme);
        this._engine.setOption(jq.evalJSON(this._options));
        var wgt = this;
        this._engine.on('click',function (params) {
            if(wgt._singleFlag) {
                var dataJson = {};
                dataJson["type"]=params.componentType;
                dataJson["name"]=params.name;
                dataJson["value"]=params.value;
                wgt.fire("onClick", {data: dataJson});
                wgt._singleFlag=false;
                setTimeout(function(){
                    wgt._singleFlag=true;//防止多次触发,且阻止全局点击
                },1000);
            }
        });
        this._engine.on('mouseover',function (params) {
            if(wgt._singleFlag) {
                var dataJson = {};
                dataJson["type"]=params.componentType;
                dataJson["name"]=params.name;
                dataJson["value"]=params.value;
                wgt.fire("onMouseOver", {data: dataJson});
                wgt._singleFlag=false;
                setTimeout(function(){
                    wgt._singleFlag=true;//防止多次触发,且阻止全局点击
                },1000);
            }
        });
        // 需要延时的操作
        if (this._getDataURL || this._getGraphItemLayouts) {
            setTimeout(function (){
                if (wgt._getDataURL) {
                    wgt.fireDataURL();
                }
                if (wgt._getGraphItemLayouts) {
                    wgt.graphItemLayouts();
                }
            }, 1000);
        }
        // 高亮目标数据
        if (this._seriesIndex >= 0 && this._dataIndex >= 0
            || this._seriesIndex.length > 0 && this._dataIndex.length > 0) {
            this.setHighLightIndex(this._seriesIndex, this._dataIndex, this._always);
        }
    },

    unbind_: function () {
        zWatch.unlisten({onSize: this});
        if (this._engine) {
            this._engine.dispose();
            this._engine = null;
        }
        this.$supers(echarts.ECharts, 'unbind_', arguments);
    }
    ,
    setEval: function (func) {

    },

    onSize: function () {
        this.$supers(echarts.ECharts, 'onSize', arguments);
        if (this._engine && (this._hflex || this._vflex)) {
            var n = this.$n();
            this._engine.resize(n.clientWidth, n.clientHeight, false);
        }
    },

    initDataURL: function () {
        this._getDataURL = true;
    },

    fireDataURL: function () {
        this.fire("onDataURL", {data: this._engine.getDataURL()});
    },

    setHighLight: function (dataIndex, always) {
        this._seriesIndex = 0;
        this._dataIndex = dataIndex;
        this._always = always;
    },

    setHighLightIndex: function (seriesIndex, dataIndex, always) {
        let myChart = this._engine;
        //设置默认选中高亮部分
        myChart.dispatchAction({
            type: 'highlight',
            seriesIndex: seriesIndex,
            dataIndex: dataIndex
        });
        if (always) {
            myChart.on('mouseout', function(e) {
                myChart.dispatchAction({
                    type: 'highlight',
                    seriesIndex: seriesIndex,
                    dataIndex: dataIndex
                });
            });
        } else {
            myChart.on('mouseout', function(e) {
                myChart.dispatchAction({
                    type: 'downplay',
                    seriesIndex: seriesIndex,
                    dataIndex: dataIndex
                });
            });
        }
    },

    initGraphItemLayouts: function () {
        this._getGraphItemLayouts = true;
    },

    graphItemLayouts: function () {
        if ('graph' !== this._engine.getOption().series[0].type) {
            return;
        }
        const cooderList = this._engine._chartsViews[0]._symbolDraw._data._itemLayouts;
        this.fire("onGraph", {data: cooderList});
    },

    clearInit: function () {
        if (this._engine) {
            this._engine.dispose();
            this._engine = null;
            this._engine = echarts.init(this.$n(), this._theme);
        }
    },

    doClick_: function () {//图表全局监听，使用echarts原生画布监听不好区分区域，修改为使用zk控件全局监听
        if(this._singleFlag) {
            var dataJson = {};
            dataJson["type"]="all";
            dataJson["name"]="echarts";
            dataJson["value"]="100%";
            this.fire("onClick", {data: dataJson});
        }
    },

    /*doMouseOver_: function () { // 图表悬浮监听
        if(this._singleFlag) {
            var dataJson = {};
            dataJson["type"]="all";
            dataJson["name"]="echarts";
            dataJson["value"]="100%";
            this.fire("onMouseOver", {data: dataJson});
        }
    }*/
})
;