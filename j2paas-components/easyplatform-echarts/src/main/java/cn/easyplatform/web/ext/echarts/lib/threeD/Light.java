/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.threeD;

import java.io.Serializable;

public class Light implements Serializable {
    /**
     * 场景主光源的设置，在 globe 组件中就是太阳光。
     */
    private Main main;
    /**
     * 全局的环境光设置。
     */
    private Ambient ambient;
    /**
     * ambientCubemap 会使用纹理作为环境光的光源，会为物体提供漫反射和高光反射。
     * 可以通过 diffuseIntensity 和 specularIntensity 分别设置漫反射强度和高光反射强度。
     */
    private AmbientCubemap ambientCubemap;

    public Main main() {
        return this.main;
    }
    public Light main(Main main) {
        this.main = main;
        return this;
    }

    public Ambient ambient() {
        return this.ambient;
    }
    public Light ambient(Ambient ambient) {
        this.ambient = ambient;
        return this;
    }

    public AmbientCubemap ambientCubemap() {
        return this.ambientCubemap;
    }
    public Light ambientCubemap(AmbientCubemap ambientCubemap) {
        this.ambientCubemap = ambientCubemap;
        return this;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public Ambient getAmbient() {
        return ambient;
    }

    public void setAmbient(Ambient ambient) {
        this.ambient = ambient;
    }

    public AmbientCubemap getAmbientCubemap() {
        return ambientCubemap;
    }

    public void setAmbientCubemap(AmbientCubemap ambientCubemap) {
        this.ambientCubemap = ambientCubemap;
    }
}
