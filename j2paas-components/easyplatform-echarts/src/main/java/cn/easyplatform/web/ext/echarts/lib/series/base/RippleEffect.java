/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series.base;

import java.io.Serializable;

/**
 * 涟漪特效相关配置
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class RippleEffect implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 涟漪的颜色，默认为散点的颜色
     */
    private String color;
    /**
     * 动画的时间
     */
    private Integer period;
    /**
     * 动画中波纹的最大缩放比例
     */
    private Double scale;
    /**
     * 波纹的填充方式，可选 'stroke' 和 'fill'
     */
    private String brushType;

    public String color() {
        return this.color;
    }

    public RippleEffect color(String color) {
        this.color = color;
        return this;
    }

    public Integer period() {
        return this.period;
    }

    public RippleEffect period(Integer period) {
        this.period = period;
        return this;
    }

    public Double scale() {
        return this.scale;
    }

    public RippleEffect scale(Double scale) {
        this.scale = scale;
        return this;
    }

    public String brushType() {
        return this.brushType;
    }

    public RippleEffect brushType(String brushType) {
        this.brushType = brushType;
        return this;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Double getScale() {
        return scale;
    }

    public void setScale(Double scale) {
        this.scale = scale;
    }

    public String getBrushType() {
        return brushType;
    }

    public void setBrushType(String brushType) {
        this.brushType = brushType;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
