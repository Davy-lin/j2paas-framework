/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ActionValueChangedVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String entityId;
	
	private String sourceName;
	
	private String targetName;
	
	private Object value;

	private String mapping;


	/**
	 * @param entityId
	 * @param sourceName
	 * @param targetName
	 * @param value
	 * @param mapping
	 */
	public ActionValueChangedVo(String entityId, String sourceName,
			String targetName, Object value, String mapping) {
		this.entityId = entityId;
		this.sourceName = sourceName;
		this.targetName = targetName;
		this.value = value;
		this.mapping = mapping;
	}

	/**
	 * @return the entityId
	 */
	public String getEntityId() {
		return entityId;
	}

	/**
	 * @return the sourceName
	 */
	public String getSourceName() {
		return sourceName;
	}

	/**
	 * @return the targetName
	 */
	public String getTargetName() {
		return targetName;
	}

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @return the mapping
	 */
	public String getMapping() {
		return mapping;
	}

}
