/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;

import cn.easyplatform.type.Constants;
import cn.easyplatform.type.FieldVo;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TaskVo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3118506048589325625L;

    /**
     * 要执行的功能id
     */
    private String id;

    /**
     * 页面打开模式
     */
    private int openModel = Constants.OPEN_NORMAL;

    /**
     * 记录CRUD代码
     */
    private String processCode;

    /**
     * <taskbox id>
     */
    private String cid;

    /**
     * 来源<taskbox from>
     */
    private String from;

    /**
     * 所属的角色
     */
    private String roleId;

    /**
     * 代理人
     */
    private String agentId;

    /**
     * 功能初始变量,外部系统调用时使用
     */
    private List<FieldVo> variables;

    /**
     * 是否需要获取环境变量
     */
    private boolean getEnv;

    public TaskVo(String id) {
        this.id = id;
    }

    public TaskVo(String id, String processCode) {
        this.id = id;
        this.processCode = processCode;
    }

    public String getId() {
        return id;
    }

    public int getOpenModel() {
        return openModel;
    }

    public void setOpenModel(int openModel) {
        this.openModel = openModel;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    /**
     * @return the cid
     */
    public String getCid() {
        return cid;
    }

    /**
     * @param cid the cid to set
     */
    public void setCid(String cid) {
        this.cid = cid;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the roleId
     */
    public String getRoleId() {
        return roleId;
    }

    /**
     * @param roleId the roleId to set
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    /**
     * @return the agentId
     */
    public String getAgentId() {
        return agentId;
    }

    /**
     * @param agentId the agentId to set
     */
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    /**
     * @return the variables
     */
    public List<FieldVo> getVariables() {
        return variables;
    }

    /**
     * @param variables the variables to set
     */
    public void setVariables(List<FieldVo> variables) {
        this.variables = variables;
    }

    public boolean isGetEnv() {
        return getEnv;
    }

    public void setGetEnv(boolean getEnv) {
        this.getEnv = getEnv;
    }
}
