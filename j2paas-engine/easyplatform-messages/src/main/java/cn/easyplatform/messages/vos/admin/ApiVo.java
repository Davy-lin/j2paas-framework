/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.admin;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/4/28 16:02
 * @Modified By:
 */
public class ApiVo implements Serializable {
    private String id;
    private String name;
    private String desp;
    private String refId;
    private List<InputVo> inputs;
    private String outputs;
    private String runAsNodes;
    private String runAsRoles;
    private String runAsUsers;
    private boolean anonymous;

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesp() {
        return desp;
    }

    public void setDesp(String desp) {
        this.desp = desp;
    }

    public List<InputVo> getInputs() {
        return inputs;
    }

    public void setInputs(List<InputVo> inputs) {
        this.inputs = inputs;
    }

    public String getOutputs() {
        return outputs;
    }

    public void setOutputs(String outputs) {
        this.outputs = outputs;
    }

    public String getRunAsNodes() {
        return runAsNodes;
    }

    public void setRunAsNodes(String runAsNodes) {
        this.runAsNodes = runAsNodes;
    }

    public String getRunAsRoles() {
        return runAsRoles;
    }

    public void setRunAsRoles(String runAsRoles) {
        this.runAsRoles = runAsRoles;
    }

    public String getRunAsUsers() {
        return runAsUsers;
    }

    public void setRunAsUsers(String runAsUsers) {
        this.runAsUsers = runAsUsers;
    }

    public boolean isAnonymous() {
        return anonymous;
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }
}
