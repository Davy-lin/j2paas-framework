/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.demo.datalist;

import cn.easyplatform.EasyPlatformRuntimeException;
import cn.easyplatform.dao.utils.DaoUtils;
import cn.easyplatform.demo.AbstractDemoTest;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.transaction.jdbc.JdbcTransactions;
import cn.easyplatform.type.EntityType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DatalistTest extends AbstractDemoTest {

	public void test1() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			log.debug("正在新建demo列表");
			conn = JdbcTransactions.getConnection(ds1);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.goods.d1");
			pstmt.setString(2, "普通列表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String datalist1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("datalist1.xml")));
			pstmt.setString(5, datalist1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.goods.d2");
			pstmt.setString(2, "多层表头");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String datalist2 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("datalist2.xml")));
			pstmt.setString(5, datalist2);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.goods.d3");
			pstmt.setString(2, "自定义查询");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String datalist3 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("datalist3.xml")));
			pstmt.setString(5, datalist3);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.goods.d4");
			pstmt.setString(2, "可编辑列表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String datalist4 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("datalist4.xml")));
			pstmt.setString(5, datalist4);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.goods.d5");
			pstmt.setString(2, "单元格计算、风格及事件");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String datalist5 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("datalist5.xml")));
			pstmt.setString(5, datalist5);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.customer.d1");
			pstmt.setString(2, "主副表页面");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String datalist6 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("customer.xml")));
			pstmt.setString(5, datalist6);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.cust_order.d1");
			pstmt.setString(2, "主副表页面");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String datalist7 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("cust_order.xml")));
			pstmt.setString(5, datalist7);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.order_detail.d1");
			pstmt.setString(2, "主副表页面");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String datalist8 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("order_detail.xml")));
			pstmt.setString(5, datalist8);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.wf_task");
			pstmt.setString(2, "待办任务");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String bpm_task = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("bpm_task.xml")));
			pstmt.setString(5, bpm_task);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.wf_cc_order");
			pstmt.setString(2, "待办任务");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String wf_cc_order = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("bpm_cc_order.xml")));
			pstmt.setString(5, wf_cc_order);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.bpm.view");
			pstmt.setString(2, "流程状态");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String bpm_view = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("bpm_view.xml")));
			pstmt.setString(5, bpm_view);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.wf_order");
			pstmt.setString(2, "流程实例");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String bpm_order = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("bpm_order.xml")));
			pstmt.setString(5, bpm_order);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.tree.1");
			pstmt.setString(2, "普通分组");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String tree1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("tree1.xml")));
			pstmt.setString(5, tree1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.tree.2");
			pstmt.setString(2, "分组编辑");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String tree2 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("tree2.xml")));
			pstmt.setString(5, tree2);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();


			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.tree.3");
			pstmt.setString(2, "上下层关系");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String tree3 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("tree3.xml")));
			pstmt.setString(5, tree3);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();
			
			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo.datalist.group.1");
			pstmt.setString(2, "分组编辑");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.DATALIST.getName());
			String group1 = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("group1.xml")));
			pstmt.setString(5, group1);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();
			
			log.debug("新建demo列表成功");
		} catch (SQLException ex) {
			throw new EasyPlatformRuntimeException("DatalistTest", ex);
		} finally {
			DaoUtils.closeQuietly(pstmt);
		}
	}
}
