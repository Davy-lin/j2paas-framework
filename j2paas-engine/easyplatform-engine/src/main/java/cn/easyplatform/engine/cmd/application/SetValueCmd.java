/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.application;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.dos.Record;
import cn.easyplatform.engine.runtime.datalist.DataListUtils;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.SetValueRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.SetValueVo;
import cn.easyplatform.messages.vos.datalist.ListSetValueVo;
import cn.easyplatform.type.IResponseMessage;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SetValueCmd extends AbstractCommand<SetValueRequestMessage> {

    /**
     * @param req
     */
    public SetValueCmd(SetValueRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        if (cc.getWorkflowContext() == null || cc.getWorkflowContext().getRecord() == null)
            return new SimpleResponseMessage();
        SetValueVo gvv = req.getBody();
        RecordContext rc = null;
        if (gvv instanceof ListSetValueVo) {
            ListSetValueVo lgvv = (ListSetValueVo) gvv;
            ListContext lc = cc.getWorkflowContext().getList(lgvv.getId());
            rc = lc.getRecord(lgvv.getKeys());
            if (lgvv.getKeys() == null) {// panel
                FieldDo field = rc.getField(gvv.getName());
                field.setRawValue(gvv.getValue());
                return new SimpleResponseMessage();
            }
            if (rc == null) {
                if (lc.isCustom()) {
                    FieldDo[] data = DataListUtils.getRow(cc, lc,
                            lgvv.getKeys());
                    Record record = new Record();
                    for (int i = 0; i < data.length; i++)
                        record.set(data[i]);
                    rc = lc.createRecord(lgvv.getKeys(), record);
                } else {
                    Record record = DataListUtils.getRecord(cc, lc,
                            lgvv.getKeys());
                    rc = lc.createRecord(lgvv.getKeys(), record);
                    lc.appendRecord(rc);
                }
            }
            // if (rc == null)
            // return MessageUtils.recordNotFound(lc.getBean().getTable(),
            // Lang.concat(lgvv.getKeys()).toString());
            if (rc.getParameterAsChar("814") != 'C')
                rc.setParameter("814", 'U');
            rc.setParameter("815", Boolean.TRUE);
        } else
            rc = cc.getWorkflowContext().getRecord();
        rc.setValue(gvv.getName(), gvv.getValue());
        return new SimpleResponseMessage();
    }

}
