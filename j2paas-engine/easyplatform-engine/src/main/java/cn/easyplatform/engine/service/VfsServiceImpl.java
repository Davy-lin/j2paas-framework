/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.service;

import cn.easyplatform.EngineService;
import cn.easyplatform.engine.cmd.vfs.*;
import cn.easyplatform.messages.request.SimpleTextRequestMessage;
import cn.easyplatform.messages.request.vfs.*;
import cn.easyplatform.spi.service.VfsService;
import cn.easyplatform.type.IResponseMessage;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class VfsServiceImpl extends EngineService implements VfsService {

    @Override
    public IResponseMessage<?> read(ReadRequestMessage req) {
        return commandExecutor.execute(new ReadCmd(req));
    }

    @Override
    public IResponseMessage<?> write(WriteRequestMessage req) {
        return commandExecutor.execute(new WriteCmd(req));
    }

    @Override
    public IResponseMessage<?> rename(RenameRequestMessage req) {
        return commandExecutor.execute(new RenameCmd(req));
    }

    @Override
    public IResponseMessage<?> copy(CopyRequestMessage req) {
        return commandExecutor.execute(new CopyCmd(req));
    }

    @Override
    public IResponseMessage<?> dir(DirRequestMessage req) {
        return commandExecutor.execute(new DirCmd(req));
    }

    @Override
    public IResponseMessage<?> delete(SimpleTextRequestMessage req) {
        return commandExecutor.execute(new DeleteCmd(req));
    }

    @Override
    public IResponseMessage<?> move(MoveRequestMessage req) {
        return commandExecutor.execute(new MoveCmd(req));
    }

    @Override
    public IResponseMessage<?> mkdir(SimpleTextRequestMessage req) {
        return commandExecutor.execute(new MkdirCmd(req));
    }
}
