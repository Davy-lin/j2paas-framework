/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.runtime.datalist;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.dao.BizDao;
import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.datalist.ListPagingVo;
import cn.easyplatform.type.Constants;
import cn.easyplatform.type.ListRowVo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ReportDataListProcess extends AbstractDataListProcess {

    @Override
    public List<ListRowVo> paging(CommandContext cc, ListPagingVo pv) {
        ListContext lc = cc.getWorkflowContext().getList(pv.getId());
        if (Constants.HIERARCHY.equalsIgnoreCase(lc.getType()))
            return super.paging(cc, pv);
        BizDao dao = null;
        if (lc.isCustom()) {
            dao = cc.getBizDao();
        } else {
            TableBean table = cc.getEntity(lc.getBean().getTable());
            dao = cc.getBizDao(table.getSubType());
        }
        StringBuilder sb = new StringBuilder();
        sb.append(lc.getSql());
        List<FieldDo> params = new ArrayList<FieldDo>();
        if (lc.getParams() != null)
            params.addAll(lc.getParams());
        if (lc.getPanelParams() != null) {
            params.addAll(lc.getPanelParams());
            sb.append(lc.getPanelQuery());
        }
        WorkflowContext ctx = cc.getWorkflowContext();
        if (!Strings.isBlank(lc.getGroupBy())) {
            sb.append(" group by ");
            String groupBy = lc.getGroupBy().trim();
            if (groupBy.startsWith("$"))
                groupBy = (String) ctx.getRecord().getValue(
                        groupBy.substring(1));
            sb.append(groupBy);
        }
        if (!Strings.isBlank(lc.getOrderBy())) {
            sb.append(" order by ");
            String orderBy = lc.getOrderBy().trim();
            if (orderBy.startsWith("$"))
                orderBy = (String) ctx.getRecord().getValue(
                        orderBy.substring(1));
            sb.append(orderBy);
        }
        List<FieldDo[]> result = dao.selectList(sb.toString(), params);
        params = null;
        sb = null;
        return createRows(cc, lc, cc.getWorkflowContext().getRecord(), result);
    }
}
