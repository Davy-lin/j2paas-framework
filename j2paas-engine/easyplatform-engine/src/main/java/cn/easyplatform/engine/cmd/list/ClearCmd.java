/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.list;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.SimpleTextRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;

import java.util.Iterator;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ClearCmd extends AbstractCommand<SimpleTextRequestMessage> {

	/**
	 * @param req
	 */
	public ClearCmd(SimpleTextRequestMessage req) {
		super(req);
	}

	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		WorkflowContext ctx = cc.getWorkflowContext();
		ListContext lc = ctx.getList(req.getBody());
		if (lc == null)
			return MessageUtils.dataListNotFound(req.getBody());
		Iterator<RecordContext> itr = lc.getRecords().iterator();
		while (itr.hasNext()) {
			RecordContext rc = itr.next();
			if (rc.getParameterAsChar("814") == 'C') {
				itr.remove();
			} else {
				lc.lock(cc, rc.getKeyValues());
				rc.setParameter("815", true);
				rc.setParameter("814", "D");
			}
		}
		return new SimpleResponseMessage();
	}

	@Override
	public String getName() {
		return "list.Clean";
	}
}
