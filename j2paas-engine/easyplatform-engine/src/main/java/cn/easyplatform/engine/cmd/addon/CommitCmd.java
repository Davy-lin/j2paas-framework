/**
 * Copyright 2019 吉鼎科技.
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.addon;

import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.SqlVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CommitCmd extends AbstractCommand<SimpleRequestMessage> {

    private static Logger log = LoggerFactory.getLogger(CommitCmd.class);

    /**
     * @param req
     */
    public CommitCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        List<SqlVo> data = (List<SqlVo>) req.getBody();
        cc.beginTx();
        try {
            for (SqlVo vo : data)
                cc.getBizDao(vo.getRid()).update(vo.getQuery(), cc.getUser(), vo.getParameters());
        } catch (Exception ex) {
            cc.rollbackTx();
            return MessageUtils.getMessage(ex, log);
        } finally {
            cc.closeTx();
        }
        return new SimpleResponseMessage();
    }

}
