/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.admin;

import cn.easyplatform.engine.cmd.vfs.DirCmd;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.request.vfs.DirRequestMessage;
import cn.easyplatform.type.IResponseMessage;

import java.io.File;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/11/6 15:35
 * @Modified By:
 */
public class GetLogCmd extends AbstractCommand<SimpleRequestMessage> {

    public GetLogCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        String dir;
        if (cc.getProjectService() == null) {
            if (req.getBody().toString().indexOf(File.separatorChar) > 0)
                dir = (String) req.getBody();
            else
                dir = cc.getEngineConfiguration().getLogPath() + "/" + req.getBody();
        } else if (req.getBody() == null || req.getBody().toString().equals("")) {
            dir = cc.getEngineConfiguration().getLogPath() + "/" + cc.getProjectService().getId();
        } else
            dir = (String) req.getBody();
        return new DirCmd(new DirRequestMessage(dir)).execute(cc);
    }
}
