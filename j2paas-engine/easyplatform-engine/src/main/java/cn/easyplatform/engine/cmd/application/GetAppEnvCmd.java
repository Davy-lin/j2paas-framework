/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.application;

import cn.easyplatform.dos.EnvDo;
import cn.easyplatform.entities.beans.project.DeviceMapBean;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.AppEnvResponseMessage;
import cn.easyplatform.messages.vos.EnvVo;
import cn.easyplatform.services.IProjectService;
import cn.easyplatform.type.DeviceType;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.EntityUtils;
import cn.easyplatform.util.MessageUtils;
import org.apache.shiro.SecurityUtils;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetAppEnvCmd extends AbstractCommand<SimpleRequestMessage> {

    /**
     * @param req
     */
    public GetAppEnvCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        if (req.getBody() instanceof EnvVo) {
            EnvVo env = (EnvVo) req.getBody();
            String contextPath = env.getAppContext();
            String deviceType = env.getDeviceType();
            IProjectService ps = cc.getProjectService(contextPath);
            if (ps == null)
                return MessageUtils.projectNotFound();
            DeviceMapBean dm = ps.getDeviceMap(env.getPortlet(), deviceType);
            if (dm == null) {
                if (deviceType.equals(DeviceType.API.getName())) {
                    dm = new DeviceMapBean();
                    dm.setType(deviceType);
                    dm.setName("ws");
                } else
                    return MessageUtils.projectDeviceInvalid();
            }
            env.setSessionId(SecurityUtils.getSubject().getSession().getId());
            env.setDeviceType(deviceType);
            env.setProjectId(ps.getId());
            env.setName(ps.getName());
            env.setTitle(ps.getDescription());
            env.setLanguages(ps.getLanguages());
            if (Strings.isBlank(dm.getTheme()))
                env.setTheme(ps.getEntity().getTheme());
            else
                env.setTheme(dm.getTheme());
            AppEnvResponseMessage resp = new AppEnvResponseMessage(env);
            cc.setupEnv(new EnvDo(ps.getId(), DeviceType
                    .getType(deviceType), ps.getLanguages().get(0),
                    env.getPortlet(), env.getVariables()));
            if (dm.getLoginPage() != null)
                env.setLoginPage(EntityUtils.parsePage(cc, dm.getLoginPage()));
            return resp;
        } else {
            EnvDo ed = cc.getEnv();
            IProjectService ps = cc.getProjectService();
            EnvVo env = new EnvVo();
            env.setDeviceType(ed.getDeviceType().getName());
            env.setProjectId(ps.getId());
            env.setName(ps.getName());
            env.setLanguages(ps.getLanguages());
            env.setSessionId(SecurityUtils.getSubject().getSession().getId());
            env.setTitle(ps.getDescription());
            env.setPortlet(ed.getPortlet());
            env.setVariables(ed.getVariables());
            env.setAppContext(ps.getEntity().getAppContext());
            DeviceMapBean dm = ps.getDeviceMap(ed.getPortlet(), env.getDeviceType());
            if (dm != null) {
                if (Strings.isBlank(dm.getTheme()))
                    env.setTheme(ps.getEntity().getTheme());
                else
                    env.setTheme(dm.getTheme());
            }
            return new AppEnvResponseMessage(env);
        }
    }

}
