/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.support.scripting;

import cn.easyplatform.ScriptEvalException;
import cn.easyplatform.ScriptEvalExitException;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.support.scripting.CompliableScriptEngine;
import cn.easyplatform.support.scripting.ScriptEngine;
import cn.easyplatform.support.scripting.ScriptEngineFactory;
import cn.easyplatform.support.scripting.ScriptEntry;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class JSScriptEngineTest extends AbstractScriptEngineTest {

	public void test1() {
		CommandContext cc = new CommandContext();
		String expression = Streams.readAndClose(Streams.utf8r(getClass()
				.getResourceAsStream("jtest1.js")));
		ScriptEngine engine = null;
		try {
			RecordContext rc = new RecordContext(createRecord(),
					createSystemVariables(), createUserVariables());
			engine = ScriptEngineFactory.createEngine()
					.init(new ScriptEntry(cc, expression,
							new RecordContext[] { rc }));
			engine.eval(1);
		} catch (ScriptEvalException ex) {
			System.err.println(String.format("exception:%d,%s", ex.getLine(),
					ex.getMessage()));
		} catch (ScriptEvalExitException ex) {
			loop(ex, cc, expression, engine.getNameSpace());
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			engine.destroy();
		}
	}

	private void loop(ScriptEvalExitException ex, CommandContext cc,
			String expression, Object ns) {
		int line = ex.getLine();
		ScriptEngine engine = ScriptEngineFactory.createEngine().init(
				new ScriptEntry(cc, expression, ns));
		try {
			engine.eval(line + 1);
		} catch (ScriptEvalExitException e) {
			loop(e, cc, expression, engine.getNameSpace());
		} finally {
			engine.destroy();
		}
	}

	public void test2() {
		CommandContext cc = new CommandContext();
		String expression = Streams.readAndClose(Streams.utf8r(getClass()
				.getResourceAsStream("batch.js")));
		CompliableScriptEngine engine = null;
		try {
			engine = ScriptEngineFactory.createCompilableEngine(cc, expression);
			RecordContext rc = new RecordContext(createRecord(),
					createSystemVariables(), createUserVariables());
			rc.getField("mySalary").setValue(1);
			for (int i = 0; i < 10; i++) {
				rc.getField("mySalary").setValue(100 * (i + 1));
				System.out.println("第" + i + "次执行：" + engine.eval(rc));
			}
		} catch (ScriptEvalException ex) {
			System.err.println(String.format("exception:%d,%s", ex.getLine(),
					ex.getMessage()));
		} finally {
			engine.destroy();
		}
	}
}
