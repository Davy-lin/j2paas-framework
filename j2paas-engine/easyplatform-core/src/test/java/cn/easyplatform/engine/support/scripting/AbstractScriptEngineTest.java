/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.support.scripting;

import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.dos.Record;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.support.scripting.RhinoScriptable;
import cn.easyplatform.type.FieldType;
import cn.easyplatform.type.ScopeType;
import org.junit.Test;
import org.mozilla.javascript.Context;

import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class AbstractScriptEngineTest {

	@Test
	public void test() {
		// CacheContext cx = CacheContext.enter();
		// try {
		// Scriptable scope = cx.initStandardObjects();
		// scope.put("$b1", scope, new TestBean<Double>("b1", 2000.3, 20));
		// scope.put("$b2", scope, new TestBean<Double>("b2", 100.31, 10));
		// String str = "$b1.print($b1.value+$b2.value)";
		// Object result = cx.evaluateString(scope, str, null, 1, null);
		// double res = CacheContext.toNumber(result);
		// System.out.println(res);
		// } finally {
		// CacheContext.exit();
		// }
		// mytest("print(1,2,3,'sss');");
		// BshScriptEngineTest btest = new BshScriptEngineTest();
		JSScriptEngineTest jtest = new JSScriptEngineTest();
		// btest.test1();
		jtest.test1();
		// jtest.test2();
		// new CalculateEngineTest().test1();
		// new NaviEngineTest().test1();
	}

	protected void mytest(String expression) {
		InputStream is = AbstractScriptEngineTest.class
				.getResourceAsStream("test.js");
		String functions = Streams.readAndClose(Streams.utf8r(is));
		Context cx = Context.enter();
		try {
			RhinoScriptable scope = new RhinoScriptable();
			scope.initStandardObjects(cx, false);
			cx.evaluateString(scope, functions, "", 1, null);
			//scope.put("cmd", scope, new MyCmd());
			cx.evaluateString(scope, expression, "", 1, null);
		} finally {
			Context.exit();
		}
	}

	/**
	 * @return
	 */
	protected Record createRecord() {
		Record record = new Record();
		FieldDo field = new FieldDo(FieldType.INT);
		field.setName("id");
		field.setValue(100);
		record.set(field);

		field = new FieldDo(FieldType.VARCHAR);
		field.setName("name");
		field.setLength(20);
		field.setValue("littleDog");
		record.set(field);

		field = new FieldDo(FieldType.DATE);
		field.setName("birthday");
		field.setValue(new Date());
		record.set(field);

		field = new FieldDo(FieldType.BOOLEAN);
		field.setName("sex");
		field.setValue(Boolean.TRUE);
		record.set(field);

		field = new FieldDo(FieldType.NUMERIC);
		field.setName("salary");
		field.setLength(20);
		field.setDecimal(2);
		field.setValue(10000.00);
		record.set(field);

		field = new FieldDo(FieldType.VARCHAR);
		field.setName("email");
		field.setLength(100);
		field.setValue("davidchen@rj-it.com");
		record.set(field);
		return record;
	}

	/**
	 * @return
	 */
	protected Map<String, FieldDo> createUserVariables() {
		Map<String, FieldDo> vars = new HashMap<String, FieldDo>();
		FieldDo field = new FieldDo(FieldType.INT);
		field.setScope(ScopeType.PROTECTED);
		field.setName("myId");
		field.setValue(100);
		vars.put(field.getName(), field);

		field = new FieldDo(FieldType.NUMERIC);
		field.setScope(ScopeType.PROTECTED);
		field.setName("mySalary");
		field.setValue(1000.00);
		vars.put(field.getName(), field);

		field = new FieldDo(FieldType.INT);
		field.setScope(ScopeType.PROTECTED);
		field.setName("age");
		field.setValue(10);
		vars.put(field.getName(), field);

		field = new FieldDo(FieldType.VARCHAR);
		field.setScope(ScopeType.PUBLIC);
		field.setName("myEmail");
		field.setValue("davidchen@rj-it.com");
		vars.put(field.getName(), field);
		return vars;
	}

	/**
	 * @return
	 */
	protected Map<String, Object> createSystemVariables() {
		Map<String, Object> vars = new HashMap<String, Object>();
		vars.put("701", "demo");
		vars.put("702","Easyplatform Demo System V5");
		vars.put("703", "demo");
		vars.put("704", "demo.ds.1");

		vars.put("720", "david");
		vars.put("721", "localhost");
		vars.put("722", "zh_CN");
		vars.put("723", new Date());
		vars.put("727", "demo.org");
		vars.put("728", "ajax");

		vars.put("781", "demo.task.1");
		vars.put("782", "测试页面功能");
		vars.put("783", "测试页面功能了");
		return vars;
	}
}
