/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.contexts;

import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.transaction.jdbc.JdbcTransactions;
import cn.easyplatform.log.LogManager;
import org.apache.shiro.util.ThreadContext;

import java.util.HashMap;
import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class Contexts {

    private static final ThreadLocal<Map<Class<?>, Object>> resources = new ThreadLocal<Map<Class<?>, Object>>() {
        @Override
        protected Map<Class<?>, Object> initialValue() {
            return new HashMap<Class<?>, Object>();
        }
    };

    public static CommandContext getCommandContext() {
        return (CommandContext) resources.get().get(CommandContext.class);
    }

    public static void set(Class<?> clazz, Object inst) {
        resources.get().put(clazz, inst);
    }

    public static <T> T get(Class<T> clazz) {
        return clazz.cast(resources.get().get(clazz));
    }

    /**
     * 销毁当前线程绑定的资源
     */
    public static void clear() {
        JdbcTransactions.clear();
        resources.remove();
        ThreadContext.remove();
        LogManager.endRequest();
    }
}
