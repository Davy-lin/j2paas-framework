/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.service;

import cn.easyplatform.messages.request.*;
import cn.easyplatform.type.IResponseMessage;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface TaskService {

    IResponseMessage<?> begin(BeginRequestMessage req);

    IResponseMessage<?> save(SaveRequestMessage req);

    IResponseMessage<?> refresh(RefreshRequestMessage req);

    IResponseMessage<?> next(NextRequestMessage req);

    IResponseMessage<?> prev(SimpleRequestMessage req);

    IResponseMessage<?> close(SimpleRequestMessage req);

    IResponseMessage<?> getFields(GetFieldsRequestMessage req);

    IResponseMessage<?> reload(ReloadRequestMessage req);

    IResponseMessage<?> getValue(GetValueRequestMessage req);

    IResponseMessage<?> setValue(SetValueRequestMessage req);

    IResponseMessage<?> getSource(SimpleRequestMessage req);

    IResponseMessage<?> action(ActionRequestMessage req);

    IResponseMessage<?> batch(ListBatchRequestMessage req);

    IResponseMessage<?> subscribe(SimpleTextRequestMessage req);

    IResponseMessage<?> unsubscribe(SimpleTextRequestMessage req);
}
