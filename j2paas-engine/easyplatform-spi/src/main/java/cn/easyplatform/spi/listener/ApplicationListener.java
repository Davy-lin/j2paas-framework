/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.listener;

import java.io.Serializable;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface ApplicationListener {

    /**
     * 分发消息
     *
     * @param projectId
     * @param topic
     * @param msg
     * @param toUsers
     */
    void publish(String projectId, String topic, Serializable msg, String... toUsers);

    /**
     * 发送消息
     *
     * @param projectId
     * @param topic
     * @param msg
     * @param toUsers
     */
    void send(String projectId, String topic, Serializable msg, String... toUsers);

    /**
     * 关闭连接
     */
    void close();
}
