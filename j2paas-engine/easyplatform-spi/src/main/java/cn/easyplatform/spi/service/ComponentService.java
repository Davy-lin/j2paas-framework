/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.service;

import cn.easyplatform.messages.request.*;
import cn.easyplatform.messages.request.addon.SaveListRequestMessage;
import cn.easyplatform.type.IResponseMessage;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface ComponentService {

    IResponseMessage<?> getList(GetListRequestMessage req);

    IResponseMessage<?> getCombo(GetComboRequestMessage req);

    IResponseMessage<?> fieldUpdate(SimpleRequestMessage req);

    IResponseMessage<?> doReport(DoReportRequestMessage req);

    IResponseMessage<?> getBpmView(GetBpmViewRequestMessage req);

    IResponseMessage<?> getBpmTask(GetBpmTaskRequestMessage req);

    IResponseMessage<?> getActionValues(ActionValueChangedRequestMessage req);

    IResponseMessage<?> upload(UploadRequestMessage req);

    IResponseMessage<?> move(MoveRequestMessage req);

    IResponseMessage<?> doMap(MapRequestMessage req);

    IResponseMessage<?> saveList(SaveListRequestMessage req);
}
