/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.type;

/**
 * 栏位变量的可选项，例如字典、掩码格式、加解密等等
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface Option {

    /**
     * 操作类型
     *
     * @return
     */
    Type getOptionType();

    /**
     * 操作值
     *
     * @return
     */
    String getOptionValue();

    public static enum Type {
        NONE,//无
        DICTIONARY;//字典

        public static Type valueOf(int type) {
            if (type == 1)
                return DICTIONARY;
            else
                return NONE;
        }
    }
}
