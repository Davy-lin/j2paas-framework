/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.lang.born;

import cn.easyplatform.lang.MatchType;

public class BornContext<T> {

	private Borning<T> borning;

	private Object[] args;

	private MatchType matchType;

	private Object lackArg;

	private Class<?>[] castType;

	public Borning<T> getBorning() {
		return borning;
	}

	public BornContext<T> setBorning(Borning<T> borning) {
		this.borning = borning;
		return this;
	}

	public Object[] getArgs() {
		return args;
	}

	public BornContext<T> setArgs(Object[] args) {
		this.args = args;
		return this;
	}

	public MatchType getMatchType() {
		return matchType;
	}

	public void setMatchType(MatchType matchType) {
		this.matchType = matchType;
	}

	public Object getLackArg() {
		return lackArg;
	}

	public void setLackArg(Object lackArg) {
		this.lackArg = lackArg;
	}

	public Class<?>[] getCastType() {
		return castType;
	}

	public void setCastType(Class<?>[] castType) {
		this.castType = castType;
	}

	public T doBorn() {
		return borning.born(args);
	}

}
