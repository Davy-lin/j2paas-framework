/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.lang.stream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class StringOutputStream extends OutputStream {

	private StringBuilder sb;
	private ByteArrayOutputStream baos;
	private String charset;

	public StringOutputStream(StringBuilder sb) {
		this(sb, "UTF-8");
	}

	public StringOutputStream(StringBuilder sb, String charset) {
		this.sb = sb;
		baos = new ByteArrayOutputStream();
		this.charset = charset;
	}

	/**
	 * 完成本方法后,确认字符串已经完成写入后,务必调用flash方法!
	 */
	@Override
	public void write(int b) throws IOException {
		if (null == baos)
			throw new IOException("Stream is closed");
		baos.write(b);
	}

	/**
	 * 使用StringBuilder前,务必调用
	 */
	@Override
	public void flush() throws IOException {
		super.flush();
		baos.flush();
		if (baos.size() > 0) {
			if (charset == null)
				sb.append(new String(baos.toByteArray()));
			else
				sb.append(new String(baos.toByteArray(), charset));
			baos.reset();
		}
	}

	@Override
	public void close() throws IOException {
		flush();
		baos = null;
	}

	public StringBuilder getStringBuilder() {
		return sb;
	}

}
