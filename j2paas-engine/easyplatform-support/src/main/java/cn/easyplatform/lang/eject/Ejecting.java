/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.lang.eject;

/**
 * 抽象取值接口
 * <p>
 * 封装了通过 getter 以及 field 两种方式获取值的区别
 * 
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a>
 */
public interface Ejecting {

	/**
	 * 通过反射，从一个对象中获取某一字段的值
	 * 
	 * @param obj
	 *            被取值的对象
	 * @return 值
	 */
	Object eject(Object obj);

}
