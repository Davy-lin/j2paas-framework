/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.lang.eject;

import cn.easyplatform.lang.Lang;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

public class EjectByField implements Ejecting {

	private static final Logger log = LoggerFactory.getLogger(EjectByField.class);

	private Field field;

	public EjectByField(Field field) {
		this.field = field;
		this.field.setAccessible(true);
	}

	public Object eject(Object obj) {
		try {
			return null == obj ? null : field.get(obj);
		} catch (Exception e) {
			if (log.isInfoEnabled())
				log.info("Fail to get value by field", e);
			throw Lang.makeThrow("Fail to get field %s.'%s' because [%s]: %s",
					field.getDeclaringClass().getName(), field.getName(),
					Lang.unwrapThrow(e), Lang.unwrapThrow(e).getMessage());
		}
	}

}
