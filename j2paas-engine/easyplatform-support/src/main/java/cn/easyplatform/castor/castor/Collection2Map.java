/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.castor.castor;

import cn.easyplatform.castor.Castor;
import cn.easyplatform.castor.FailToCastObjectException;
import cn.easyplatform.lang.Lang;

import java.util.Collection;
import java.util.Map;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class Collection2Map extends Castor<Collection, Map> {

	@Override
	public Map cast(Collection src, Class<?> toType, String... args)
			throws FailToCastObjectException {
		if (null == args || args.length == 0)
			throw Lang
					.makeThrow(
							FailToCastObjectException.class,
							"For the elements in Collection %s, castors don't know which one is the key field.",
							src.getClass().getName());
		return Lang.collection2map((Class<Map<Object, Object>>) toType, src,
				args[0]);
	}

}
