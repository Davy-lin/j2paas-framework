
package cn.easyplatform.report.chart;

import java.util.HashMap;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

import org.junit.Test;

import cn.easyplatform.report.AbstractReportTest;
import cn.easyplatform.report.JREasyPlatformScriptlet;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Pie3DChartTest extends AbstractReportTest {

	@Test
	public void test1() throws Exception {
		JasperReport jr = JasperCompileManager
				.compileReport(Pie3DChartTest.class
						.getResourceAsStream("pie3d_1.jrxml"));
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("REPORT_CONNECTION", getConn());
		parameters.put("REPORT_SCRIPTLET", new JREasyPlatformScriptlet());
		parameters.put("gender", Boolean.TRUE);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jr, parameters);
		JasperViewer viewer = new JasperViewer(jasperPrint);
		viewer.setVisible(true);
	}

	@Test
	public void test2() throws Exception {
		JasperReport jr = JasperCompileManager
				.compileReport(Pie3DChartTest.class
						.getResourceAsStream("pie3d_2.jrxml"));
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("REPORT_CONNECTION", getConn());
		parameters.put("REPORT_SCRIPTLET", new JREasyPlatformScriptlet());
		parameters.put("amount", 0d);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jr, parameters);
		JasperViewer viewer = new JasperViewer(jasperPrint);
		viewer.setVisible(true);
		synchronized (this) {
			this.wait();
		}
	}
}
