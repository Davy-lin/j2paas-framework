/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.list;

import cn.easyplatform.entities.helper.FieldTypeAdapter;
import cn.easyplatform.type.FieldType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlType(propOrder = { "name", "field", "title", "description", "type", "sql",
		"sqlSeparator", "isSort", "width", "format", "component", "isVisible",
		"totalType", "totalName", "groupName", "style", "cellStyle",
		"totalStyle", "groupStyle", "event", "align", "valign", "iconSclass",
		"image", "hoverimg","draggable" })
@XmlAccessorType(XmlAccessType.NONE)
public class Header implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2060357531776050837L;

	@XmlElement(required = true)
	private String name;// 列标识号,当field为空时，可以在计算表达式使用这个标识,组成表达式[a-zA-Z0-9_]

	@XmlElement
	private String field;// 栏位名称,与表栏位对应，组成表达式[a-zA-Z0-9_],约定：如果field不为空，name必须与field一致

	@XmlElement
	private String title;// 标题

	@XmlElement
	private String description;

	@XmlJavaTypeAdapter(value = FieldTypeAdapter.class)
	@XmlElement
	private FieldType type;// 数据类型

	@XmlElement
	private String sql;// 来源,select name from table where
						// id=$id，一般用在自定义查询,field并不存在本表中

	@XmlElement
	private String sqlSeparator;// 值是有多个分隔符组成的字符串,配合sql使用

	@XmlElement
	private boolean isSort;// 是否显示排序功能

	@XmlElement(required = true)
	private String width;// 列宽

	@XmlElement
	private String format;// 格式

	@XmlElement
	private String component;// 显示的组件

	@XmlElement
	private boolean isVisible = true;// 是否可视

	@XmlElement
	private int totalType;// 统计类型,取代isTotal：1-表示汇总;2-平均;3-表示笔数;4-最大值;5-最小值

	@XmlElement
	private String totalName;// 统计的名称

	@XmlElement
	private String style;// 表头风格

	@XmlElement
	private String cellStyle;// 单元格风格

	@XmlElement
	private String totalStyle;// 统计的风格

	@XmlElement
	private String groupName;// 分组时的名称

	@XmlElement
	private String groupStyle;// 分组的风格

	@XmlElement
	private String event;// 事件表达式

	@XmlElement
	private String image;// 显示在列头的图标

	@XmlElement
	private String align;

	@XmlElement
	private String valign;

	@XmlElement
	private String iconSclass;

	@XmlElement
	private String hoverimg;

	@XmlElement
	private String draggable;

	public String getDraggable() {
		return draggable;
	}

	public void setDraggable(String draggable) {
		this.draggable = draggable;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public FieldType getType() {
		return type;
	}

	public void setType(FieldType type) {
		this.type = type;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public boolean isSort() {
		return isSort;
	}

	public void setSort(boolean isSort) {
		this.isSort = isSort;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}

	public int getTotalType() {
		return totalType;
	}

	public void setTotalType(int totalType) {
		this.totalType = totalType;
	}

	public String getTotalName() {
		return totalName;
	}

	public void setTotalName(String totalName) {
		this.totalName = totalName;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getCellStyle() {
		return cellStyle;
	}

	public void setCellStyle(String cellStyle) {
		this.cellStyle = cellStyle;
	}

	public String getTotalStyle() {
		return totalStyle;
	}

	public void setTotalStyle(String totalStyle) {
		this.totalStyle = totalStyle;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupStyle() {
		return groupStyle;
	}

	public void setGroupStyle(String groupStyle) {
		this.groupStyle = groupStyle;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getAlign() {
		return align;
	}

	public void setAlign(String align) {
		this.align = align;
	}

	public String getValign() {
		return valign;
	}

	public void setValign(String valign) {
		this.valign = valign;
	}

	public String getIconSclass() {
		return iconSclass;
	}

	public void setIconSclass(String iconSclass) {
		this.iconSclass = iconSclass;
	}

	public String getHoverimg() {
		return hoverimg;
	}

	public void setHoverimg(String hoverimg) {
		this.hoverimg = hoverimg;
	}

	public String getSqlSeparator() {
		return sqlSeparator;
	}

	public void setSqlSeparator(String sqlSeparator) {
		this.sqlSeparator = sqlSeparator;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{").append(field).append(",").append(type).append(",")
				.append(title).append("}");
		return sb.toString();
	}

}
